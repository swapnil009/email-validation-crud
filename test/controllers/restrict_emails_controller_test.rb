require 'test_helper'

class RestrictEmailsControllerTest < ActionController::TestCase
  setup do
    @restrict_email = restrict_emails(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:restrict_emails)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create restrict_email" do
    assert_difference('RestrictEmail.count') do
      post :create, restrict_email: {  }
    end

    assert_redirected_to restrict_email_path(assigns(:restrict_email))
  end

  test "should show restrict_email" do
    get :show, id: @restrict_email
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @restrict_email
    assert_response :success
  end

  test "should update restrict_email" do
    patch :update, id: @restrict_email, restrict_email: {  }
    assert_redirected_to restrict_email_path(assigns(:restrict_email))
  end

  test "should destroy restrict_email" do
    assert_difference('RestrictEmail.count', -1) do
      delete :destroy, id: @restrict_email
    end

    assert_redirected_to restrict_emails_path
  end
end
