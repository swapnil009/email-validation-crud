class RestrictEmailsController < ApplicationController
 # before_action :set_restrict_email, only: [:show, :edit, :update, :destroy]

  # GET /restrict_emails
  # GET /restrict_emails.json
  def index
    @restrict_emails = RestrictEmail.all
  end

  # GET /restrict_emails/1
  # GET /restrict_emails/1.json
  def show
  end

  # GET /restrict_emails/new
  def new
    @restrict_email = RestrictEmail.new
  end

  # GET /restrict_emails/1/edit
  def edit
  end

  # POST /restrict_emails
  # POST /restrict_emails.json
  def create
    
    @restrict_email = RestrictEmail.new(restrict_email_params)
    email_array= params[:restrict_email][:blocked_email].split(',')
    new_hash=[]
    email_array.map{|email_string| new_hash.push({blocked_email:email_string})}
    RestrictEmail.create(new_hash)
    
    redirect_to restrict_emails_url
    # respond_to do |format|
    #   if @restrict_email.save
    #     format.html { redirect_to @restrict_email, notice: 'Restrict email was successfully created.' }
    #     format.json { render :show, status: :created, location: @restrict_email }
    #   else
    #     format.html { render :new }
    #     format.json { render json: @restrict_email.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # PATCH/PUT /restrict_emails/1
  # PATCH/PUT /restrict_emails/1.json

  def destroy_multiple
  respond_to do |format|
    format.html { redirect_to blog_posts_path }
    format.json { head :no_content }
  end

end


  def update
    respond_to do |format|
      if @restrict_email.update(restrict_email_params)
        format.html { redirect_to @restrict_email, notice: 'Restrict email was successfully updated.' }
        format.json { render :show, status: :ok, location: @restrict_email }
      else
        format.html { render :edit }
        format.json { render json: @restrict_email.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /restrict_emails/1
  # DELETE /restrict_emails/1.json
  def destroy
map_ids=params[:blocked_email].map{|id| id.split('-')[1].to_i} 
 RestrictEmail.delete_all(["id in (?)", map_ids])
    respond_to do |format|
      format.html { redirect_to restrict_emails_url, notice: 'Restrict email was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_restrict_email
      @restrict_email = RestrictEmail.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def restrict_email_params
      params.require(:restrict_email).permit(:blocked_email)
    end
end
