json.extract! restrict_email, :id, :created_at, :updated_at
json.url restrict_email_url(restrict_email, format: :json)
