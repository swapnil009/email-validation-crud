class CreateRestrictEmails < ActiveRecord::Migration
  def change
    create_table :restrict_emails do |t|
      t.string :blocked_email
      t.timestamps null: false
    end
  end
end
